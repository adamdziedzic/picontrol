# you have to open connection somewhere
import mariadb
import pickle

import numpy as np
import time
import datetime
import matplotlib.pyplot as plt

conn = mariadb.connect(
    user="picontrol",
    password="picontrolpassword",
    host="localhost",
    database="picontrol")


# every action triggered by frontend is represented by a row in tasks table.
def fetch_tasks():
    with conn.cursor() as cur:
        cur.execute("DELETE FROM tasks RETURNING id, type, deviceName")
        # cur.execute("SELECT id, type, deviceId, deviceName FROM tasks;")
        for (task_id, task_type, device_name) in cur:
            if task_type == 'RemoveDevice':
                print("removing " + device_name)
                # removeDevice(device_name, task_id)
            elif task_type == 'StartLearning':
                print("learning " + device_name)
                # learningDevice(device_name, task_id)
            elif task_type == 'StartProgram':
                print("starting program")
                # startProgram(task_id)
            elif task_type == 'StopProgram':
                print("stopping program")
                # stopProgram(task_id)
            elif task_type == 'ResetProgram':
                print("resetting program")
                # resetProgram(task_id)
        conn.commit()


class Device(object):

    def __init__(self, name, active, learning_active):
        self.name = name
        self.active = active
        self.learning_active = learning_active


# if you want to insert devices (right now by overriding everything)
def insert_devices(devices_list):
    with conn.cursor() as cursor:
        cursor.execute("DELETE FROM devices;")
        for device in devices_list:
            state = 'active' if device.active else 'inactive'
            learning_state = 'active' if device.learning_active else 'inactive'
            cursor.execute("INSERT INTO devices (name, state, learning_state) VALUES (?, ?, ?)",
                           (device.name, state, learning_state))
        conn.commit()


def get_devices():
    with conn.cursor() as cursor:
        cursor.execute("SELECT name, state, learning_state FROM devices;")
        for (name, state, learning_state) in cursor:
            print(f"Found device {name}, {state}, {learning_state}")


def send_log(log_content):
    with conn.cursor() as cursor:
        cursor.execute("INSERT INTO logs (content) VALUES (?)", (log_content,))
    conn.commit()


def remove_all_logs():
    with conn.cursor() as cursor:
        cursor.execute("DELETE FROM logs;")
    conn.commit()


def insert_total_power_data(date, totalPowerHistory):
    dictionary = {'date': date, 'totalPowerHistory': totalPowerHistory}
    with conn.cursor() as cursor:
        cursor.execute(
            "INSERT INTO image_data (image_name, data_dictionary) VALUES (?, ?) ON DUPLICATE KEY UPDATE data_dictionary = VALUES (data_dictionary)",
            ('plot_total_power', pickle.dumps(dictionary)))
    conn.commit()


def insert_disaggregated_power_data(date, stateHistory, label):
    dictionary = {'date': date, 'stateHistory': stateHistory, 'label': label}
    with conn.cursor() as cursor:
        cursor.execute(
            "INSERT INTO image_data (image_name, data_dictionary) VALUES (?, ?) ON DUPLICATE KEY UPDATE data_dictionary = VALUES (data_dictionary)",
            ('plot_disaggregated_power', pickle.dumps(dictionary)))
    conn.commit()


def insert_energy_consumption_data(label, energyConsumption):
    dictionary = {'label': label, 'energyConsumption': energyConsumption}
    with conn.cursor() as cursor:
        cursor.execute(
            "INSERT INTO image_data (image_name, data_dictionary) VALUES (?, ?) ON DUPLICATE KEY UPDATE data_dictionary = VALUES (data_dictionary)",
            ('plot_energy_consumption', pickle.dumps(dictionary)))
    conn.commit()


def insert_figure_legend_data(label):
    dictionary = {'label': label}
    with conn.cursor() as cursor:
        cursor.execute(
            "INSERT INTO image_data (image_name, data_dictionary) VALUES (?, ?) ON DUPLICATE KEY UPDATE data_dictionary = VALUES (data_dictionary)",
            ('figure_legend', pickle.dumps(dictionary)))
    conn.commit()


def generateAndSavePlotData():
    x = np.arange(0, 100);
    plt.close("all")
    y = np.random.rand(100) * 2500

    state = np.random.randint(low=0, high=1, size=(100, 5))
    state[20:30, 1] = 1
    state[25:45, 0] = 1
    state[55:78, 2] = 1
    state[65:69, 3] = 1
    state[88:99, 4] = 1
    stateHistory = state * np.array([1950, 1000, 60, 1500, 2100])
    label = ['kettle', 'heater', 'bulb', 'dryer', 'iron']

    energyConsumption = np.array([2, 0.25, 0.02, 4, 6])
    actTime = time.time_ns()
    startTime = actTime - 31e9
    timestamp = np.linspace(startTime, actTime, y.shape[0])
    date = []
    for t in timestamp:
        date.append(datetime.datetime.fromtimestamp(t / 1e9))

    insert_total_power_data(date, y)
    insert_disaggregated_power_data(date, stateHistory.transpose(), label)
    insert_energy_consumption_data(label, energyConsumption)
    insert_figure_legend_data(label)


# generateAndSavePlotData()
# fetch_tasks()
# insert_devices([
#     Device("some name", True, False),
#     Device("some name 2", False, False),
#     Device("some name 3", False, True)
# ])
# send_log("some log")
# send_log("some log1")
# send_log("some log2")
# send_log("some log3")
# send_log("some log4")
# remove_all_logs()
get_devices()


# and after your work is done, you have to close connection
conn.close()
