import base64

from database import get_image_data
from figurTest2 import plotTotalPower, plotDisaggregatedPower, plotEnergyConsumption, figureLegend


def image_with_name(image_name):
    data_dictionary = get_image_data(image_name)
    if data_dictionary is None:
        return ""
    figure = plot_with_name(image_name, data_dictionary)
    data = base64.b64encode(figure.getbuffer()).decode("ascii")
    return "data:image/png;base64," + data


def plot_with_name(image_name, data_dictionary):
    if image_name == "plot_total_power":
        return plotTotalPower(data_dictionary['date'], data_dictionary['totalPowerHistory'])
    elif image_name == "plot_energy_consumption":
        return plotEnergyConsumption(data_dictionary["label"], data_dictionary["energyConsumption"])
    elif image_name == "plot_disaggregated_power":
        return plotDisaggregatedPower(data_dictionary["date"], data_dictionary["stateHistory"] ,data_dictionary["label"])
    elif image_name == "figure_legend":
        return figureLegend(data_dictionary["label"])
