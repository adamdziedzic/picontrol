async function fetchAsync (url) {
    let response = await fetch(url);
    return await response.json();
}

async function postAsync (url, body) {
    let response = await fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body)
    });
    return await response.json();
}

async function deleteAsync (url) {
    let response = await fetch(url, {method: 'DELETE'});
    return await response.json();
}

function getBadge(state) {
    switch (state) {
        case "inactive":
            return "<span class=\"btn-floating grey\"></span>";
        case "active":
            return "<span class=\"btn-floating pulse red\"></span>";
        default:
            return "";
    }
}

function postSync(url, body, callback) {
    document.getElementById("loading-indicator").classList.remove('hide');
    document.getElementById("loading-indicator1").classList.remove('hide');
    postAsync(url, body)
        .then(data => {
            document.getElementById("loading-indicator").classList.add('hide');
            document.getElementById("loading-indicator1").classList.add('hide');
            callback(data);
        });
}

function startLearning(deviceName, btn) {
    document.getElementById(btn.id).disabled = true;
    console.log(`learning ${deviceName}`)
    postSync(`/devices/${deviceName}/start-learning`, {}, (data) => {})
}

function removeDevice(deviceName, btn) {
    document.getElementById(btn.id).disabled = true;
    console.log(`removing ${deviceName}`)
    document.getElementById("loading-indicator").classList.remove('hide');
    deleteAsync(`/devices/${deviceName}/`)
        .then(data => {
            document.getElementById("loading-indicator").classList.add('hide');
        })
}

function startProgram() {
    postSync(`/program/start`, {}, (data) => {})
}

function stopProgram() {
    postSync(`/program/stop`, {}, (data) => {})
}

function resetProgram() {
    postSync(`/program/reset`, {}, (data) => {})
}

function getActions(deviceId, deviceName) {
    return `<button class="btn waves-effect waves-light with-margin" onClick='startLearning("${deviceName}", this)' id='start-${deviceId}'>Start learning</button>` +
        `<button class="btn waves-effect waves-light with-margin" onClick='removeDevice("${deviceName}", this)' id='remove-${deviceId}'>Remove device</button>`;
}

function loadLogs() {
    fetchAsync("/logs/")
        .then(data => {
            const logsDiv = document.getElementById("logs-content");
            logsDiv.innerHTML = "";
            data.map((line) => {
                logsDiv.innerHTML += line + "<br>";
            });
            logsDiv.scrollTop = logsDiv.scrollHeight;
        });
}

function loadPlots() {
    getImage("total-power-plot", "/image/plot_total_power")
    getImage("disaggregated-power-plot", "/image/plot_disaggregated_power")
    getImage("energy-consumption-plot", "/image/plot_energy_consumption")
    getImage("legend-figure", "/image/figure_legend")
}

function getImage(elementId, endpoint) {
    const totalPowerPlotImg = document.getElementById(elementId);
    fetchAsync(endpoint)
        .then(data => {
            totalPowerPlotImg.src = data["imageContent"];
        });
}

function loadDevices() {
    fetchAsync("/devices/")
        .then(data => {
            const devicesDiv = document.getElementById("devices-table");
            const devicesShortDiv = document.getElementById("devices-short-table");
            devicesDiv.innerHTML = "";
            devicesShortDiv.innerHTML = "";
            data.map((device) => {
                // devicesDiv.innerHTML += `<td>${device["name"]}</td><td>${getBadge(device["learning_state"])}</td><td>${getActions(device['id'], device['name'])}</td>`;
                devicesDiv.innerHTML += `<td>${device["name"]}</td><td>${getActions(device['id'], device['name'])}</td>`;
                devicesShortDiv.innerHTML += `<td>${getBadge(device['state'])}</td><td>${device['name']}</td>`
            })}
        );
}
