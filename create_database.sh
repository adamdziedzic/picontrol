#!/bin/bash
MAINDB="picontrol"
PASSWDDB="picontrolpassword"
mysql -uroot -e "CREATE DATABASE ${MAINDB} /*\!40100 DEFAULT CHARACTER SET utf8 */;"
mysql -uroot -e "CREATE USER ${MAINDB}@localhost IDENTIFIED BY '${PASSWDDB}';"
mysql -uroot -e "GRANT ALL PRIVILEGES ON ${MAINDB}.* TO '${MAINDB}'@'localhost';"
mysql -uroot -e "FLUSH PRIVILEGES;"
mysql -uroot "${MAINDB}" < schema.sql