from flask import Flask, jsonify, request, redirect
from flask import render_template

from database import Device, Task, TaskType, save, LogEntry, get_all
from figure_handling import image_with_name

app = Flask(__name__,
            static_url_path='',
            static_folder='static')


# with app.app_context():
#     from database import Base, engine
#
#     Base.metadata.drop_all(engine)
#     Base.metadata.create_all(engine)


@app.route("/")
def index():
    return render_template('index.html')


@app.route("/logs/", methods=["GET"])
def show_logs():
    logs = map(lambda entry: entry.content, get_all(LogEntry) or [])
    return jsonify(list(logs))


@app.route("/image/<image_name>", methods=["GET"])
def images_endpoint(image_name):
    return jsonify({"imageContent": image_with_name(image_name)})


@app.route("/devices/", methods=["GET"])
def devices_endpoint():
    return jsonify(get_all(Device))


@app.route("/devices/new/start-learning", methods=["POST"])
def new_device_endpoint():
    app.logger.warning(request.form['device_name'])
    save(Task(TaskType.StartLearning, request.form['device_name']))
    return redirect("/")


@app.route("/devices/<device_name>/start-learning", methods=["POST"])
def start_learning(device_name):
    app.logger.warning("learning" + device_name)
    save(Task(TaskType.StartLearning, device_name))
    return jsonify({})


@app.route("/devices/<device_name>/", methods=["DELETE"])
def remove(device_name):
    app.logger.warning("removing" + device_name)
    save(Task(TaskType.RemoveDevice, device_name))
    return jsonify({})


@app.route("/program/start", methods=["POST"])
def startProgram():
    app.logger.warning("starting program")
    save(Task(TaskType.StartProgram, None))
    return jsonify({})


@app.route("/program/stop", methods=["POST"])
def stopProgram():
    app.logger.warning("stopping program")
    save(Task(TaskType.StopProgram, None))
    return jsonify({})


@app.route("/program/reset", methods=["POST"])
def resetProgram():
    app.logger.warning("resetting program")
    save(Task(TaskType.ResetProgram, None))
    return jsonify({})
