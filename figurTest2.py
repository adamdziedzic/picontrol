# -*- coding: utf-8 -*-
"""
Created on Fri Sep 16 21:26:52 2022

@author: augus
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Sep  9 17:08:23 2022

@author: augus
"""
from io import BytesIO
import matplotlib.pyplot as plt
from matplotlib.figure import Figure

import numpy as np
import matplotlib.dates as dates
import time
import datetime


colors=['tab:blue',
    'tab:orange',
    'tab:green',
    'tab:red',
    'tab:purple',
    'tab:brown',
    'tab:pink',
    'tab:gray',
    'tab:olive',
    'tab:cyan']


def plotTotalPower(date, totalPowerHistory):
    
    fig=Figure(figsize=(7,4))
    ax = fig.subplots()
    ax.plot(date,totalPowerHistory,'b',linewidth=2)
    ax.title.set_text("Total Power")
    ax.set_xlabel("Time elapsed [s]")
    ax.set_ylabel("P [W]")
    ax.grid(True)
    loc=dates.SecondLocator(bysecond=None, interval=120)
    ax.xaxis.set_major_locator(loc) 
    #creating binary to send
    buffer=BytesIO()
    fig.savefig(buffer, format='png')
    # with open("figure1.png", "wb") as f:
    #     f.write(buffer.getbuffer())
    
    return buffer

def plotDisaggregatedPower(date, stateHistory,label):
    fig=Figure(figsize=(7,4))
    ax = fig.subplots()
    for i in range(0,stateHistory.shape[0]):    
        ax.plot(date,stateHistory[i,:],linewidth=2,label=label[i],color=colors[i%10])
    loc=dates.SecondLocator(bysecond=None, interval=120)
    ax.set_title("Disaggregated Power")
    ax.set_xlabel("Time elapsed [s]")
    ax.set_ylabel("P [W]")
    ax.grid(True)
    ax.xaxis.set_major_locator(loc) 
    #locs=plt.xticks()
    #ax.set_xlim(locs[0][0],locs[0][-1])
    #creating binary to send
    buffer=BytesIO()
    fig.savefig(buffer, format='png')
    
    # with open("figure2.png", "wb") as f:
    #     f.write(buffer.getbuffer())
    
    return buffer

def plotEnergyConsumption(label, energyConsumption):
    fig=Figure(figsize=(7,4),constrained_layout=True)
    ax = fig.subplots()
    ax.set_title("Energy consumption per appliance")
    ax.set_ylabel("[kWh]")
    ax.bar(np.arange(len(label)),energyConsumption,tick_label=label)
    ax.set_xticklabels(label, rotation=45)
    #creating binary to send
    buffer=BytesIO()
    fig.savefig(buffer, format='png')
    
    # with open("figure3.png", "wb") as f:
    #     f.write(buffer.getbuffer())
    
    return buffer

def figureLegend(label):
    fig=Figure()
    ax = fig.subplots()
    handles=[]
    for i in range(0,len(label)):
        h=ax.plot([],[],marker="s",ls="none",color=colors[i%10])[0]
        handles.append(h)
    figLegend = Figure(figsize=(2, 3), dpi=100)
    figLegend.legend(handles, label,loc='upper center', framealpha=1, frameon=False)
    #rect=legend.get_frame()
    #figLegend.set_figheight(rect.get_height())
    #figLegend.set_figwidth(rect.get_width())
    buffer=BytesIO()   
    figLegend.savefig(buffer, format='png')

    # with open("figure4.png", "wb") as f:
    #     f.write(buffer.getbuffer())
    
    return buffer

x=np.arange(0,100);
plt.close("all")
y=np.random.rand(100)*2500

state=np.random.randint(low=0, high=1, size=(100,5))
state[20:30,1]=1
state[25:45,0]=1
state[55:78,2]=1
state[65:69,3]=1
state[88:99,4]=1
stateHistory=state*np.array([1950,1000,60,1500,2100])
label=['kettle', 'heater','bulb','dryer','iron']

energyConsumption=np.array([2,0.25,0.02,4,6])  
actTime=time.time_ns()
startTime=actTime-31e9
timestamp=np.linspace(startTime, actTime, y.shape[0])
date=[]
for t in timestamp:
    date.append(datetime.datetime.fromtimestamp(t/1e9))
 
plotTotalPower(date, y)
plotDisaggregatedPower(date, stateHistory.transpose(),label)    
plotEnergyConsumption(label, energyConsumption)
figureLegend(label)