# installing MySQL
follow the instruction:
https://www.digitalocean.com/community/tutorials/how-to-install-the-latest-mysql-on-debian-10

Don't do "Step 3 — Securing MySQL". For development purposes we don't need that

# Creating database
run `./create_database.sh`

# Run db on system startup
`sudo systemctl start mysql`

# install dependencies
`source venv/bin/activate`
`pip3 -r requirements.txt`

# Run an app
`python3.9 -m flask --app=main run --host=0.0.0.0`

# Launch app on system startup
```
sudo cp ./nialm.service /etc/systemd/system/
sudo systemctl enable nialm
```

## Managing app after
```
sudo systemctl status nialm - check current app status
sudo systemctl start nialm - start app if not running
sudo systemctl stop nialm - stop app
journalctl -u nialm - check logs
```

Replace nialm with webpage for the same with frontend app

# If you want app to be online
## Install diode
```
curl -Ssf https://diode.io/install.sh | sh
diode publish -public 5000:80
```

## Alternative 
installation of nginx and manual configuration of some domain to point to your raspberry-pi.

