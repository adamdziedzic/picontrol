from flask_sqlalchemy import SQLAlchemy
import enum
from sqlalchemy import Enum
from dataclasses import dataclass
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from contextlib import closing

db = SQLAlchemy()

engine = sqlalchemy.create_engine("mariadb+mariadbconnector://picontrol:picontrolpassword@localhost/picontrol")
Base = declarative_base()


class DeviceState(str, enum.Enum):
    active = "active"
    inactive = "inactive"


@dataclass
class Device(Base):
    __tablename__ = 'devices'
    id: int = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    name: str = sqlalchemy.Column(sqlalchemy.String(200))
    state: DeviceState = sqlalchemy.Column(Enum(DeviceState))
    learning_state: DeviceState = sqlalchemy.Column(Enum(DeviceState))

    def __init__(self, name, state, learning_state):
        self.name = name
        self.state = state
        self.learning_state = learning_state

    def __repr__(self):
        return '<Device %r>' % self.name


class TaskType(str, enum.Enum):
    StartLearning = "start_learning"
    RemoveDevice = "remove_device"
    StartProgram = "start_program"
    StopProgram = "stop_program"
    ResetProgram = "reset_program"


@dataclass
class Task(Base):
    __tablename__ = 'tasks'
    id: int = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    type: TaskType = sqlalchemy.Column(Enum(TaskType), nullable=False)
    deviceName: str = sqlalchemy.Column(sqlalchemy.String(200))

    def __init__(self, type, deviceName):
        self.type = type
        self.deviceName = deviceName

    def __repr__(self):
        return '<Task %r>' % self.id


@dataclass
class LogEntry(Base):
    __tablename__ = 'logs'
    id: int = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    content: str = sqlalchemy.Column(sqlalchemy.String(2000))

    def __init__(self, content):
        self.content = content

    def __repr__(self):
        return '<LogEntry %r>' % self.id


@dataclass
class ImageData(Base):
    __tablename__ = 'image_data'
    image_name: str = sqlalchemy.Column(sqlalchemy.String(200), primary_key=True)
    data_dictionary: str = sqlalchemy.Column(sqlalchemy.PickleType(impl=sqlalchemy.LargeBinary(length=(2**32)-1)))

    def __init__(self, image_name, data_dictionary):
        self.image_name = image_name
        self.data_dictionary = data_dictionary

    def __repr__(self):
        return '<Image Data %r>' % self.image_name


Session = sqlalchemy.orm.sessionmaker()
Session.configure(bind=engine)


def save(new_entity):
    with closing(Session()) as session:
        session.add(new_entity)
        session.commit()


def get_all(entity_class):
    with closing(Session()) as session:
        return session.query(entity_class).all()


def get_image_data(image_name):
    with closing(Session()) as session:
        row = session.query(ImageData).filter(ImageData.image_name == image_name).first()
        return row.data_dictionary if row else None
